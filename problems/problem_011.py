# Complete the is_divisible_by_5 function to return the
# word "buzz" if the value in the number parameter is
# divisible by 5. Otherwise, just return the number.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


#thoughts:
# looks like its the same as the previous problem
# so i guess i can copy, paste, change some variables
# ezpz

# original:
# def is_divisible_by_5(number):
#     pass

def is_divisible_by_5(number):
    divisible = number % 5
    if number == 0:
        return "Cannot use zero! Choose a different number"
    elif divisible == 0:
        return "No remainders, number is divisible by three! Buzz!"
    else:
        return number


# marked as solved

# before looking at solution:
# nothing much to add

# after looking at solution:
# oh the solution seems to be shorter than the previous solution
# its not they just didnt put the comments
# eyes are not functioning properly
# next problem i go
# i wonder if i can just remove the print
# after much deliberation between me, myself, and i
#i will remove
