# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

# def calculate_average(values):
#     pass

#thoughts:
# getting the average is adding the numbers(sum) and
# dividing it by the amount of said nnumbers.
# WHERES THE PSEUDO CODE.
# if its a list i dont see it, hack reactor people
# i need an empty list to append the numbers
# the length of the list
# if the len of the list is 0, it should return none
# #lessgetit.
# can i append a list of numbers or should i concatenate it?


#pseudo:

def calculate_average(values):
    sum_of_values = sum(values)
    if len(values) > 0:
        return sum_of_values / len(values)
    else:
        None

# MARKED AS SOLVED

print(calculate_average([333, 2352, 2342, 3, 323, 9583]))

# before looking at solution:
# put my code into thonny and its wrong
# i think i have the write idea but the argument
# is a list, which i think is causing me trouble
# on second thought i dont need to append to a list
# thats already given to me if
# i dont need to add on to that list
# okay redid my code and it looks like it works fine
# checking the solution

# after looking at solution:
# its definitely different but it works exactly like mine,
# though the use of a for loop is interesting. i think its
# only been used because we learned about it
# before learning what 'sum()' does
# and i only know because of a quick google search
# eithe way it works so ill mark it as solved


def calculate_average(values):
    if len(values) == 0:
        return None
    sum = 0
    for item in values:
        sum = sum + item
    return sum / len(values)
