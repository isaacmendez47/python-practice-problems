# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.



# thoughts
# im using linter and its kinda annoying to have
# this is going to be another if statement with
# and as a hard case
# maybe 2 ifs? i dont think so but lets see
# if the person can only have a consent for
# doesnt that mean anyone can go as long as
# they have a consent form? age <= 2 can work
# would that mean i need to have has_consent_form as a boolean?

# pseudo
# age has to be equal to or greater than 18 >=
# OR must have a consent form
# can_skydive is true


def can_skydive(age, has_consent_form):
    if age >= 18 or has_consent_form:
        return "Good to go in the air!"
    else:
        return "stay on the ground, shrimp!"


# marked as solved


# before looking at solution:
# looks like the checks pass and babies can fly
# if they have consent forms. well, good for them i guess

# after looking at solution:
# noice
