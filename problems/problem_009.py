# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

##
# thoughts:
# i think im going to have to use a method to
# reverse a word and check it to the original
# to see if its the same backwards as it is forwards
# do i have to make a list for it? i dont have to
# iterate, just reverse a string and check that to itself

# pseudo:
# aknowledges that an argument is a word
# word is then reversed
# if the reversed word is equal to word
# it is a palindrome
# if not, not a  palindrome

def is_palindrome(word):
    reversed_word = word[::-1]
    if reversed_word == word:
        return "Palindrome!"
    else:
        return "No palindrome found here"


# ill mark it in solved


# thoughts before looking at solution:
# i had to look up how to reverse a string
# good thing is its not a method that i need to use
# something a tad more complicated
# i wonder if i can import a dictionary
# there are words called emordnilap that exist
# and i would for this function to check if those exists

# thoughts after looking at solution:
# 'reversed' is a keyword that makes an objec that needs to be called
# which is where .join comes in
# the solution calls for a '.join()' method to be used
# im going to debug it in thonny to see the step by step of the solution
# after looking at it in thonny the reversed word is stored in the quotes
# thats kinda cool

# questions about the proposed solution:
# is there a need for the empty quotes?
    #why doesnt it workd if theres a space inside of the quote?
    # putting instead 'word' and using racecar as an argument makes it false
