# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


# thoughts:

# there needs to be an if statement function comparing two values
# returns the lowest value
# if else if to the rescue. i see we can do ternary operations,
# maybe to refactor i can use that but for now i'll mark it as solved
# doesnt say we should print it so i wont put a print statement
# in the function or outside of it
# when i refactor i can try and make sure that its a
# only numbers and print out an error if theres characters that are not numbers


def minimum_value(value1, value2):
    if value1 == value2:
        return ("neither")
    elif value1 > value2:
        return (value2)
    else:
        return (value1)

# marked as solved

# looking at the solution i didnt need them to compare themselves to each other
# and check that theyre equal but it still gives me the same result. if i want
# things to look cleaner though ill delete it
