# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# original:
# def can_make_pasta(ingredients):
#     pass

# thoughts
# ah, lists. my dear enemy. i shall figure you out
# so i can to make a for loop to check whats in a list
# can i make a variable with the string of items
# i need checked inside of the list?
# those sentences make me feel like an engineer lol
# if statements?

#pseudo:
# make variable equal to a list containing a string
# string is flour eggs and oil , seperately
# if the items in the list are in the list, return true
# else, return false

def can_make_pasta(ingredients):
    my_ingredients = ["flour" , "eggs" , "oil"]
    for index in my_ingredients:
        if index == ingredients:
            return "true"
        else:
            return "false"

print(can_make_pasta("flour" , "eggs" , "oil"))

#NOT SOLVED
# before looking at solution:
# i gave up on this one thinking i can do it with for loops. it seems possible
# but i need to get a better grip on my not taking.

# after looking at solution:
# the 'in' keyword was barely taught so i dont see it in my notes
# the solution is simple tho, so ill keep working on mine
