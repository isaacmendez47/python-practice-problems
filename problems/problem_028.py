# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

# original:
# def remove_duplicate_letters(s):

# thoughts"
# immediately know that its going to be a 'for' loops
# i need to iterate a list that removes all duplicate letter
# and returns a string with all the duplicates removed
# so if s == s_removed? i guess i dont even know how to approach any of these
# does that mean i have to access the index of the index?
# i guessi have to make sure the string is in a list?



def remove_duplicate_letters(s):
    new_string = []
    if new_string == []:
        return new_string
    for letter in s:
        if letter in s:
            new_string.append(letter)
    return new_string




# TRIED AND FAILED



print(remove_duplicate_letters("aabbccddeeffgg"))

# before looking at solution:
#  yeah i have no idea what to do with this one
# going to look at the solution for 27 to see how the for loops
# work, i forgot already
# man even using the solution to 27 as a basis this problem i dont get much
# i still have to remember that identation plays a huge
# role in how functions work.
# i got it to print out every letter in a string, now i have to get it to
# print out the string with duplicates removed
# i have to look, i have no idea about this one

# after looking at solution:
# i dont think i would have gotten this
# i knew i was going the right way with 'not in'
# but i couldnt see what to do with the extra letters
# i see what its doing, its doing the same thing i wanted but i was
# doing it differenly
# i dont need a list if its a string. # im wrong
