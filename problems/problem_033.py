# Complete the sum_of_first_n_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the n is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def sum_of_first_n_even_numbers(n):
#     pass

# thoughts
# this looks like the other one but the steps of range
# should be changed from 1 to 2 since it has to skip every
# other integer
# copy and paste and lets see what happens
# so that did not work and using 1 as an argument returns 2,
# using 2 returns 6, but using 5 returns 12?
# i think i have to play with the paramets in range
# ill be back
# should i use a modulo or multiply the n by 2?

def sum_of_first_n_numbers(n):
    if n < 0:
        return None
    total = 0
    for number in range(0 , (n * 2) + 1  , 2):
        total += number
    return total

# MARKED AS SOLVED

print(sum_of_first_n_numbers(2))

# thoughts before looking at solution:
# so it looks like i was right with the ranges and i have come
# up with 'range(0 , (n *2) +1 , 2 )
# i didnt have to add a module which is great for me and all the
# conditions they have given me have been met

# after looking at solution:
# all i had to do was change the total sum. i went the
# more complicated route, which according to the python
# law is not correct, but the result is still the same for
# the conditions met.
# marked as solved
