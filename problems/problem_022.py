# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

# originial
# def gear_for_day(is_workday, is_sunny):
#     pass

# thoughts:
# looks like i need a couple of parameters that include:
# a list of days, from sunday to saturday
# if the day is sunny or not sunny
# the list of gear needs to include: umbrella, laptop, surfboard
# im having trouble processing this. idk why
# wait do we need to append to an empty list?
# if i use a list to check what day it is do i have to use 'in' to
# check the lisr?
# maybe a variable saying whats a weekend and whats a weekday
# i guess a parameter to check the weather
# after my shower and rereading it i think i can get away with two
# parameters: day and weather.
# i can append the gear in the 'if' statements
# into an empty list. the list needs to be returned
# looks like i need a 'not' keyword
# the parameters were already given. D'oh!
# im confused as to how many items should be in a list


# pseudo:
# def if statement (day , weather)
# not sunny, append "umbrella"
# if not sunny and workday, need "umbrella"
# if               workday, need "laptop"
# if           not workday, need "surfboard"

def gear_for_day(is_workday, is_sunny):
    workday = ["monday" , "tuesday" , "wednesday" , "thursday" , "friday"]
    sunny = "sunny"
    gear_umbrella = "umbrella"
    gear_laptop = "laptop"
    gear_surfboard = "surfboard"
    gear_for_today = []
    if is_workday in workday and sunny is not is_sunny:
        gear_for_today.append(gear_umbrella)
        return gear_for_today
    elif is_workday in workday:
        gear_for_today.append(gear_laptop)
        return gear_for_today
    else:
        gear_for_today.append(gear_surfboard)
        return gear_for_today

# MARKED AS SOLVED

print(gear_for_day("sunday" , "cloudy"))


# thoughts before looking at solution:
# i dont think this is right at all.
# i'll rewrite when i check
# for a first time write this worked quite well
# i think i need to add an item to something. not sure what though
# i think one of the lists needs to contain both
# a laptop and an umbrella
# im goint to have to look at the problem in a bit after # reading it
# but for now im think 3 lists, 1 if that nests 2 other ifs

# after looking at solution:
# i think i did remarkbly well.
# i added more variables but the outcome is still the same
# i aslo managed to misspell surfboard twice so ill fix now.
# ill mark it solved
