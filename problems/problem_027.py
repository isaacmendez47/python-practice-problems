# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

# original:
# def max_in_list(values):
#     pass


# thoughts:
# looks like im going to iterate through
# a list and find the highest value
# comparing each value.
# i can make 2 variables, one for highest so far
# and current number to be checked
# and if the current is higher than the highest
# the highest = current number, else go back to loop
# i dont know anything about while loops
# but this would be a great case to use them in

# pseudo:
# two variables current_highest , compared_to
# if compared_to > highest_number
# return highest_number = compared_to
# both variables start off at zero

def max_in_list(values):
    values.sort()
    highest_value = 0
    for numbers in values:
        if numbers > highest_value:
            highest_value = numbers
            return values[len(values) - 1]
        else:
            return None

# somewhat solved?


print(max_in_list([45, 3, 78, 34, 90, 98, 99, 4, 109]))

# before looking at solution:
# maybe i dont need a second variable set to zero
# ill run it without the second variable
# i forgot returns end a function, so maybe two ifs?
# how do i get it to loop even if the previous number is higher
# than the next number?
# idea, maybe a sort function exists?
# seems like the sort workds but i need to get the last index.
# return values[len(values)] maybe?
# the way i did it worked but my god is it weird. i dont think i need
# a for loop the way i did it
# time for a looksie, since it works well enough.
# keyword, enough

# after looking at solution:
# ive decided that if i think my code works well before
# checking the solution, if i want to change anything
# ill add it before this second
# i definitely did it wonky and not the way they wanted me to
# debugging it in thonny has made me realize that some solutions
# like this one, maybe you need to add a return statement BEFORE
# for example, if it 0 then end
# i was really close in figuring out the loop
# but i think if i flipped it it might have worked better
# marked as solved, kinda
# weird combination of my google research and what i learned
# i also learned you dont need an 'else' with 'if' statements
