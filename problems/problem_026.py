# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

# def calculate_grade(values):
#     pass


# Thoughts:
# i think i might need to check if each value is
# higher than or equal to 0
# lower than or equal to 100
# students can get both a 100 and 0 in real life tests
# then theres some pseudo code provided for us
# i need to get the sum of the list and then divide that list
# by the amount of values that list has
# let me do the ifs first and check the solution to see
# if they have anything about checking

def calculate_grade(values):
    average = sum(values) / len(values)
    if average >= 90:
        return "A"
    elif average < 90 and average >= 80:
        return "B"
    elif average < 80 and average >= 70:
        return "C"
    elif average < 70 and average >= 60:
        return "D"
    else:
        return "F"

# MARKED AS SOLVED

print(calculate_grade([35, 46, 90, 99, 97, 99, 89]))

# before looking at solution:
# idk if they want us to check if the arguments are going to only be 0-100 but
# i guess i will now in a minute

# after looking at solution:
# they did not so i guess im lucky there
# code is different but works the same
# ill mark as solved
# i def put a lot more tho, but its more explicit
