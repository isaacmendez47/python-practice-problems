# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


# thoughts:
# this looks the same as finding if a number
# is divisible by 2
# divisible and dividing something are two different things


# pseudo:
# take a number, any number
# if its divisible, no remainders
# if the modulo is bigger than 0
# then its cant be divisible by 3



def is_divisible_by_3(number):
    divisible = number % 3
    if number == 0:
        return "Cannot use zero! Choose a different number"
    elif divisible == 0:
        return "No remainders, number is divisible by three! Fizz"
    else:
        return number


# marked as solved


# before looking at solution:
# i think this is appropriate so far. i tried to have an edge case for 0
# solution look time

# after looking at solution:
# woops i forgot it has to say fix
# im trying to make it more complex than i should, i stopped
# okay i deleted code because i didnt follow the rules and need
# to stop making it more complicated for now until im sure
# i can do it
