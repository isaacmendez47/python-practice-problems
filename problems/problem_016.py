# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

# original
# def is_inside_bounds(x, y):
#     pass

# thoughts:
# looks like another if statement using 'in'
# is 'within a keyword'?
# do i have to iterate each number one to 10?
# or can i do range
# it has to test each so i guess i can use 'and'?

# pseudo:

# if x is in 0-10. inclusive
# and if y is in 0-10, inclusive,
# print something. "within range", works i guess

#1 integers only
def is_inside_bounds(x, y):
    within_range = range(10)
    if x in within_range and y in within_range:
        return "both are in range!"
    else:
        return "one or both of the numbers is bigger than 10!"

#2 integers and floats
def is_inside_bounds(x, y):
    if x >= 0 and x < 10 and y >=0 and y is < 10:
        return "both are in range!"
    else:
        return "one or both of the numbers is bigger than 10!"



# mark as solved:

# only works with integers
# using equal to greater than/less than operands
# is a better solution

# before looking at solution:
# this tests if both numbers are bigger than 10
# im going to say it satisfys it

# after looking at solution:
# okay wait the solution is pretty cool
# more concise because i added a variable
# within_range doesnt have to be used
# but the if line is longer.
# nonetheless i think its both the same output
# i just tried it with floats with my solution,
# if either is a float it says its bigger
# if either is a float it says its out of range. interesting
# so range doesnt include floats? just integers
