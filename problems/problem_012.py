# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by
#   by both 3 and 5
# * The word "fizz" if number is evenly divisible by only
#   3
# * The word "buzz" if number is evenly divisible by only
#   5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# thoughts:
# looks like im going to have to compare the same number twice
# to a modulo
# im going to have to use the 'and' keyword and some 'if'statements'
# the pseudo is pretty much already done for us. boo
# can i use 'or' checks? if one is false it and one is true it should kick
# the one down the chain. or my brain isnt working


#pseudo:
# check if a number is divisible by both 3 and 5
# if divisible by 3 AND 5, return fizzbuzz
# if it is is divisble by 3, return fizz
# if it is divisible by 5, returnm buzz
# if it is not divisible by 3 and 5, return the number

def fizzbuzz(number):
    if number % 3 == 0 and number % 5 == 0:
        return "fizzbuzz"
    elif number % 3 == 0:
        return "fizz"
    elif number % 5 == 0:
        return "buzz"
    else:
        return number

# marked as solved

# before looking at solution:
# this one i felt pretty confident. grammar errors to the side, not bad
# i have to remember quotes for strings

# after looking at solution:
# its pretty much the same. proud of myself :)
