# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

# original:
# def has_quorum(attendees_list, members_list):
#     pass

# thoughts:
# looks like a simple math check.
# does this mean we cant use floats since
# half a person cant exist? or we cant really count
# someone as 'half a person'?

# pseudo:
# if attendees_list >= members_list / 2
# i guess i can return true
# it doesnt tell me to return true or false

def has_quorum(attendees_list, members_list):
    if len(attendees_list) >= len(members_list) / 2 :
        return "true":
    else:
        return "false"

# NOT SOLVED> NEEDS WORK

print(has_quorum(50 / 24))

# before looking at solution:
# this one seems too easy as well but we shall see
# running the solution with thonny i think i got the
# 'if' statements switched

# after looking at solution:
# my reading comprehension skills arent here right now
# it needs to get the data from a list, so i need the length
# of the list to know how many people are coming
# so would that mean i need to pass in a list to make sure it works correctly?
# two lists, one for atteendees, and one for members
# [isaac, gloria, jonathan] , [mateo, jeremias, fatima, isabela]
# after looking at the solution i again after fixing my code my
# reading comprehension is definitely not here.
# or i dont understand whats written.
# oh i see where i went wrong. ill mark it as not solved though and come
# back to it later
