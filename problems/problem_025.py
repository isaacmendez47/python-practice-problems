# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

# original:
# def calculate_sum(values):
#     pass

# thoughts:
# so im kinda giddy at this because i know what the
# previous problem entailed with. should i do it with a for loop?
# or do it with a 'sum()'?
# also why isnt this problem 24 and the previous problem 25? makes no sense

# pseudo:
# :)

def calculate_sum(values):
    if values == []:
        return None
    else:
        return sum(values)

# MARKED AS SOLVED

print(calculate_sum([]))

# thoughts before looking at solution:
# this seems like a no brainer 5 code solution
# watch this be completely wrong tho
# running the code into thonny with 0 arguments besides
# an empty lists return '0'. why?
# ill change the if statement and see what happens
# if i change the if statement to say 'values == []'
# it prints none
# ill check the solution now

# after looking at solution:
# i see why they are choosing that if statement.
# if len() of a list is empty / 0
# itll prit none
# mine only does if its empty.
# wouldnt mine be better
# if the values were negative?
# though with strings it wont work as well
# maybe i can raise an error if not an number?
# ill mark it as solved
