# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# original:
# def sum_of_squares(values):
#     pass

# thoughts:
# i need to get the values of whats in a list, so i need to iterate
# those values, power them by themselves, and add those squares together.
# at first glance this seems pretty easy but idk anymore with challeneges
# lessgitit
# i need better earphones, these are starting to hurt


#pseudo
# for ever number in a list of values
# square that number by itself
# then add those numbers together
# all of them just ADD ALL OF THEM
# woo 11:32 coding.
# do i need to appened to an empty list or an empty list

def sum_of_squares(values):
    total_sum += 0

    if len(values) == 0:
        return None
    for number in values:
        total_sum = number * number
        return sum_of_squares



# NOT SOLVED COME BACK TO THIS ONE

print(sum_of_squares([1 , 2 , 3 ])

# before looking at solution:
# this one might be harder than it looks
# i feel like this one should be obvious but im not seeing it for some reason
# i dont really see how its any different than the previous problem
# i think i got the right idea, its not appending tho
#i have no idea whats going on with this problem at all
# i might need to access every index of the list?

# why does this not work:
#def sum_of_squares(values):
#     if len(values) == 0:
#         return None
#     lst_of_powered = []
#     summed = sum(lst_of_powered)
#     for number in values:
#         powered_num = number ** number
#         lst_of_powered.append(powered_num)


#     return sum_of_squares

# print(sum_of_squares([1 , 2 , 3 ]))

# after looking at solution:
# im over here thinking its powers when its multiplication.
# i still dont understand how i dont get this. i was heading in the
# right direction
