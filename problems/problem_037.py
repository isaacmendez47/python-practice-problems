# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"

#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"

#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"

#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

# original:
# def pad_left(number, length, pad):
#     pass

# thoughts:
# i dont know why this was so difficult for me to read but it was weirdly
# worded, at least in my opinion.
# so it looks like i need to get the length of the string
# and if the string is less than the length suggested, i need to add a padding
# i think in terms of strings, strings can be concatenated with other strings
# so can i add an empty variable that can be added to it using a for loop?
# do i need to do a for loop? what am i looping?
# do i need range? it seems almost silly to use it here
# it doesnt seem like i need to use an if statement here.
# do i need turn it the number into a string when its being put?
# and then from there  do i need to add the padding?
# to know the amount of padding to put it needs to know the amount of characters
# in the number minus the length of the length is greater than the
# characters in the number in string form
# do i just need to multipy the string of pad and add it to the
# string of a number? cant i just multiple the subtracted ber


#
# pseudo:
# if the length of string is < than the length specified,
# return the number in string
# for i in some_variable
# if len(number) > length
#   amount_of_padding_to_add = number - length
#

def pad_left(number, length, pad):
    str_number = str(number)
    if len(str_number) > length:
        return str_number
    elif len(str_number) < length:
        subtract = length - len(str_number)
        pad_added_and_string = (pad * subtract) + str(number)
        return pad_added_and_string

# MARKED AS SOLVED

print(pad_left(10, 4, "*"))



# before looking at solution:
# i can multiply
# i completely changed the code and
# its so DIFFERENT but it seems to work.
# the new one i dont need to use a for loop
# one if statement could work i think, but i used an if and elif
# all the checks passed that were given so im going to count it
# as solved but im curious to the solution they have given me

# after looking at solution:
# the solution has a while loop
# i think i might have missed a couple lessons so ill go and see if theres
# a class im missing
# i dont see anything about while loops, just for loops, but ill keep while
# loops in the back of my mind as a solution
# the checks pass so ill mark it as solved
# i need to get better at psedo code, i spent like 3 hours on this one problem
