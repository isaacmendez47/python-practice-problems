# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#original:
# def find_second_largest(values):
#     pass


# thoughts:
# this is almost like the one that is trying
# to find the largest number in a list, but this one is trying
# to find the second largest.
# i think my first thought is to get 2 variables for this one.
# one variable to get the highest, and another variable to compare it to
# they both start at zero and as it iterates
# if  'a > b ' with a being higher, than a is highest number
# i have a basic understanding but i think i can get it
# i think i can compare it to solution 27 but im going to try and
# do it from memory. last night is a blur
# i think im doing good with two variables to compare against.
# not going to know exactly until the end

#pseudo:
# if values = []
#    return "none"
# highest value is set to zero
# second highest is set to zero
# for num in values
#

def find_second_largest(values):
    if len(values) == 0 or len(values) == 1:
        return None
    highest_value = 0
    second_highest = 0
    for number in values:
        if number > highest_value:
            highest_value = number
    for number in values:
        if number < highest_value and number > second_highest:
            second_highest = number

    return second_highest

# marked as solved

print(find_second_largest([45, 3, 78, 34, 90, 98, 99, 4, 109, 65, 45, 86, 223, 567, 76, 4, 456, 974]))

# before looking at solution:
# okay i think i got the first step without looking, retrieving the highest.
# now i need to get the second highest
# thanks to thonny i know whats wrong with my code but i dont know what to do YET
# i like this problem
# maybe a not?
# i could do a sort() and get the second to last index like i did before
# if i want to append a number to another list i can just
# sort from the beginning, and i dont want to do that
# do i have to do two for loops?
# okay oh my GOD after spending like 3 decades on this problem i think
# i finally got it.
# i think i went about it in a weird way but lets see what the
# solutuion has in store

# after looking at solution:
# I KNEW I COULD USE ONE FOR LOOP AND IF ELIF STATEMENT BUT I COULDNT
# FIGURE OUT THE INDENTATION. Well now i know that i dont have to close
# any if statements with a return or an 'else' at the end.
# im going to mark mine as solved sinve it achieves the same thing
