# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


# thoughts
# i have to take 3 variables and compare them to each other. can i use 'and' or 'or'? my first thoughts are 'i dont think so but ill think about it'
# it seems like its like the last problem so i can take inspiration from how i solved it
# do we have to make a list for this?

# psedo

# 3 variables to see whats the maximum
# if all three are the same, return one of the values
# if 2 are the same, return one of the values
# there has to be an if to compare the 3
# in the end ill have to call on max_of_three(x , x2 , x3)
# so i can put that at the bottom with a print statement

# after thoughts
# looking at the solution for 1 second i did see
# that i can use 'and' as a way to solve it

def max_of_three(value1, value2, value3):
    if value1 >= value2 and value1 >= value3:
        return value1
    elif value2 >= value1 and value2>= value3:
        return value2
    else:
        return value3



#marked as solved
