# Complete the count_letters_and_digits function which
# accepts a parameter s that contains a string and returns
# two values, the number of letters in the string and the
# number of digits in the string
#
# Examples:
#   * "" returns 0, 0
#   * "a" returns 1, 0
#   * "1" returns 0, 1
#   * "1a" returns 1, 1
#
# To test if a character c is a digit, you can use the
# c.isdigit() method to return True of False
#
# To test if a character c is a letter, you can use the
# c.isalpha() method to return True of False
#
# Remember that functions can return more than one value
# in Python. You just use a comma with the return, like
# this:
#      return value1, value2

# thoughts:
# the argument is going to be a string, not a list
# this is a weird one
# is it going to be dictionaries?
# to check if each character is a string i need to put it inside
# the foor loop


def count_letters_and_digits(s):
    total_letters = 0
    total_numbers = 0

    for i in s:
        if  i.isdigit():
            total_numbers += 1
        if  i.isalpha():
            total_letters +=1
    return total_letters , total_numbers

# MARKED AS SOLVED

print(count_letters_and_digits("S1u2p3e4r5c6a7l8i9f1r2a3g4i5l6i7s8t9i1c2e3x4p5i6a7l8i9d1o2c3i4o5u6s7"))

# before looking at solution:
# okay it seems like i got it first try so im proud of myself for that
# TIL the word i used has 34 characters and none of 34 contain b, h, j, k, m, n ,q ,v, w, y, z

# after looking at solution
# sweet, just like mine so definitely solved
