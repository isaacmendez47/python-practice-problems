# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of the rectangle
#   rect_y: The bottom of the rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

# ORIGINAL
# def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
#     pass

# Thoughts:
# damn thats a lot of checks.
# seems like all the psuedo steps are somewhat written
# down already so ill start with that.
# i dont understand what the coordinate is
# theres 6 parameters, damn
#

#pseudo:
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    if x >= rect_x and y >= rect_y and x <= (rect_x + rect_width) and y <= (rect_y + rect_height):
        return "true"
    else:
        return "false"

# MARKED AS SOLVED

print(is_inside_bounds())

#STOPPIN HERE FOR PROBLEMS CLASSES 66

# before looking at solution:
# i dont know about this one, it seems too easy.
# every single check needs to be true or all is false,
# which is only 4 checks with 6 arguments

# after looking at solution:
# okay yay it was basically what i had but more readable. ill count it as solved
