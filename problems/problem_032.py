# Complete the sum_of_first_n_numbers function which accepts
# a numerical limit and returns the sum of the numbers from
# 0 up to and including limit.
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+1=1
#   * 2 returns 0+1+2=3
#   * 5 returns 0+1+2+3+4+5=15
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# original:
# def sum_of_first_n_numbers(limit):
#     pass

# thoughts:
# it looks like i need to add all the numbers together?
# it doesnt say anything from a list so maybe its a string?
# im going to assume its a list for now
# oh i see what its doing. its only taking ONE
# argument thats not a list
# im going to try and not to do lists since lists are the bane
# of my existence

def sum_of_first_n_numbers(limit):
    if limit < 0:
        return None
    total = 0
    for number in range(0, limit + 1, 1):

        total += number
    return total

print(sum_of_first_n_numbers(0))

# MARKED AS SOLVED

# thoughts before looking at solution:
# so it looks like i found the solution by accident?
# i can use 'range()' and if i want to add the argument itself as well
# i have to use 'range(start, stop + 1, step)'
# and replace start with 0, stop with the argument, and step
# with 1 since i want to all all whole integers between
# 0 and 5
# solution look time

# thoughts after looking at soultion:
# almost the same, tho their range doesnt include the start and step
# which i guess is essentially the same
# ill mark it as solved
